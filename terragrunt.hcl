remote_state {
    backend = "azurerm"
    config = {
        key = "${path_relative_to_include()}/terraform.tfstate"
        resource_group_name = "hux-rg-dev"
        storage_account_name = "huxsgacdev"
        container_name = "tfstate"
    }
}
inputs = {
location = "East US"
prefix = tgmodule
}
