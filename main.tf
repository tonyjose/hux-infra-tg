provider "azurerm" {
  features {}
}
terraform {
  backend "azurerm" {}
}
module "network" {
  source = "./az-network"
}
module "resource_grp" {
  source = "./az-rg"
  location = "westus"
}