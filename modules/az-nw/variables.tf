variable "location" {
  description = "Location for the resources"
}
variable "prefix" {
  description = "Prefix added to the resources"
}